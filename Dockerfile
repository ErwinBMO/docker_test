FROM mcr.microsoft.com/powershell:windowsservercore-1903
ADD ./mcr_installer /mcr_installer
RUN c:/mcr_installer/bin/win64/setup.exe -mode silent -agreeToLicense yes
ENTRYPOINT powershell.exe

# mount the repo folder inside the folder to c:\data so you don't have to import the exe or export the result file from helloWorld
# >docker run --rm -i -v C:\Users\Damir\GIT\docker_test:c:\data matlabtest




# Escape newline character within dockerfile so that colons can be used as newline identifiers in the dockerfile (choice between frontslash and colon, but the former is banned by windows)
# escape=`

# Normal servercore image suffices as well.
# FROM mcr.microsoft.com/powershell:windowsservercore-1903
# FROM mcr.microsoft.com/powershell:lts-nanoserver-1903

# launches a powershell interactive terminal
#ENTRYPOINT powershell.exe

# Install MatLab runtime.
# ADD ./mcr_installer /mcr_installer
# RUN c:/mcr_installer/bin/win64/setup.exe -mode silent -agreeToLicense yes
# ENTRYPOINT cmd.exe


#RUN powershell \ 
  #mkdir /mcr -Force >$null;\
  ##Start-Process mcr_installer/bin/win64/setup.exe -ArgumentList '-mode silent -agreeToLicense yes -destinationFolder C:/mcr' -Wait;\
  #Remove-Item mcr_installer -Force

# Hello world .exe simply does disp('hello world!');pause(5). Available in the matlabscripts folder on server_HA
# ADD ./helloWorld.exe /matlab/helloWorld.exe



# 1. In case you want to install matlab from the already downloaded zip archive, uncomment following lines.
# 2. Comment 'install matlab from the web' lines above.
#
# NOTE: MCR_xxxxxx_installer.exe has to be located in the save directory as Dockerfile.

#ADD ./MCR_xxxxxx.installer.exe /mcr-xxxx_installer.exe
#RUN powershell -Command `
#    Start-Process MCR_R2013b_win64_installer.exe -ArgumentList "-mode silent -agreeToLicense yes" -NoExit ;