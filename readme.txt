This Dockerfile assumes the following executables to be downloaded and present in the root of this repository
* helloWorld.exe
* MCR_R2013b_win64_installer.exe

To build and run the Dockerfile, type
* docker build .
* docker run -i <container_id>